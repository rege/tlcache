package tlcache

import (
	"fmt"
	"gitlab.com/rege/vgenerator"
	"testing"
	"time"
)

// create a disk cache with specified count of items
func makeSomeDiskCache(count int) (diskCache *diskCache, err error) {
	diskCache, err = newDiskCache("test_cache")
	if err != nil {
		err = fmt.Errorf("got error initializing disk cache: %s\n", err)
		return
	}
	for i := 0; i < count; i++ {
		items := map[string]Item{
			fmt.Sprintf("item%d", i): {
				time.Now().Add(time.Second).UnixNano(),
				vgenerator.GenRandomValue(),
			},
		}
		err = diskCache.Put(items)
		if err != nil {
			return nil, fmt.Errorf("got error by diskCache.Put(%v): %s\n", items, err)
		}
	}
	return
}

func BenchmarkDiskCache_Put(b *testing.B) {
	diskCache, err := newDiskCache("test_cache")
	defer diskCache.Flush()

	if err != nil {
		b.Fatalf("got error initializing disk cache: %s\n", err)
	}

	b.RunParallel(func(pb *testing.PB) {
		for i := 0; pb.Next(); i++ {
			items := map[string]Item{
				"item0": {
					time.Now().Add(time.Second).UnixNano(),
					vgenerator.GenRandomValue(),
				},
				"item2": {
					time.Now().Add(time.Second).UnixNano(),
					vgenerator.GenRandomValue(),
				},
				"item3": {
					time.Now().Add(time.Second).UnixNano(),
					vgenerator.GenRandomValue(),
				},
			}
			err = diskCache.Put(items)
			if err != nil {
				b.Errorf("got error by diskCache.Put(%v): %s\n", items, err)
			}
			if i%1000 == 0 {
				diskCache.Flush()
			}
		}
	})
}

func TestCache_CountOnDisk(t *testing.T) {
	targetCount := 20

	diskCache, err := makeSomeDiskCache(targetCount)
	if err != nil {
		t.Fatal(err)
	}

	got := diskCache.Count()
	if targetCount != got {
		t.Errorf("diskCache.Count() = %d; want %d\n", got, targetCount)
	}

	diskCache.Flush()
}

func TestDiskCache_GetItem(t *testing.T) {
	diskCache, err := newDiskCache("test_cache")
	if err != nil {
		t.Fatalf("got error initializing disk cache: %s\n", err)
	}

	itemsMap0 := map[string]Item{
		"item0": {
			time.Now().Add(time.Second).UnixNano(),
			123,
		},
		"item1": {
			time.Now().Add(time.Second).UnixNano(),
			"item1",
		},
	}
	itemsMap1 := map[string]Item{
		"item2": {
			time.Now().Add(time.Second).UnixNano(),
			true,
		},
	}

	err = diskCache.Put(itemsMap0)
	if err != nil {
		t.Errorf("got error by diskCache.Put(%v): %s\n", itemsMap0, err)
	}
	err = diskCache.Put(itemsMap1)
	if err != nil {
		t.Errorf("got error by diskCache.Put(%v): %s\n", itemsMap1, err)
	}
	err = diskCache.Put(itemsMap1)
	if err != nil {
		t.Errorf("[double call] got error by diskCache.Put(%v): %s\n", itemsMap1, err)
	}

	type testData struct {
		arg string
		want interface{}
	}
	testDataList := []testData{{"item0", 123},{"item1", "item1"}, {"item2", true}}

	for _, testItem := range testDataList {
		t.Run(testItem.arg, func(t *testing.T) {
			got, err := diskCache.GetItem(testItem.arg)
			if err != nil {
				t.Fatalf("got error by diskCache.GetItem(%s): %s", testItem.arg, err)
			}
			var equals bool
			switch testItem.want.(type) {
			case int:
				equals = int(got.Value.(float64)) == testItem.want.(int)
			case string:
				equals = got.Value.(string) == testItem.want.(string)
			case bool:
				equals = got.Value.(bool) == testItem.want.(bool)
			default:
				panic("somebody forget about this code")
			}
			if !equals {
				t.Fatalf("diskCache.GetItem(%s) gives %v; want %v", testItem.arg, got.Value, testItem.want)
			}
		})
	}
}
