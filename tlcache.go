package tlcache

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"reflect"
	"sort"
	"time"
)

// errors
// note: Many other not unifiable error can be throwed.
var (
	notFound         = errors.New("not found")
	itemTimeIsOver   = errors.New("item time is over")
	invalidValueType = errors.New("invalid value type")
)

type Item struct {
	Expiration int64
	Value      interface{}
}

type regularItem struct {
	LastUse int64
	Item
}

type Cache struct {
	defaultDuration time.Duration
	cleanupInterval time.Duration
	ramCache        *ramCache
	diskCache       *diskCache
}

func New(defaultExpiration, cleanupInterval time.Duration, maxCountInRAM int, directory string) (*Cache, error) {
	ramCache := newRamCache(maxCountInRAM)
	diskCache, err := newDiskCache(directory)
	if err != nil {
		return nil, fmt.Errorf("cannot init a disk cache: %s", err)
	}

	cache := &Cache{
		defaultDuration: defaultExpiration,
		cleanupInterval: cleanupInterval,
		ramCache:        ramCache,
		diskCache:       diskCache,
	}
	go cache.garbageCollector()

	return cache, nil
}

// WARNING: if cache item was in disk cache, often value of item will have another type
// that is broken to more simple (after marshalling to json and unmarshalling back)
func (c *Cache) GetRawItem(key string) (*Item, error) {
	item, err := c.ramCache.GetItem(key)
	if err != nil {
		if err == notFound {
			item, err = c.diskCache.GetItem(key)
			if err != nil {
				return nil, err
			}
			c.ramCache.Put(key, item)
			return item, nil
		}
		return nil, err
	}
	return item, nil
}

func (c *Cache) Delete(key string) {
	c.ramCache.Delete(key)
	c.diskCache.Delete(key)
}

func (c *Cache) Put(key string, value interface{}) {
	c.ramCache.Put(key, &Item{
		Expiration: time.Now().Add(c.defaultDuration).UnixNano(),
		Value:      value,
	})
	c.performForceRecachingIfNecessary()
}

func (c *Cache) PutForever(key string, value interface{}) {
	c.ramCache.Put(key, &Item{
		Expiration: 0,
		Value:      value,
	})
	c.performForceRecachingIfNecessary()
}

func (c *Cache) Flush() {
	c.ramCache.Flush()
	c.diskCache.Flush()
}

func (c *Cache) PutWithDuration(key string, value interface{}, duration time.Duration) {
	c.ramCache.Put(key, &Item{
		Expiration: time.Now().Add(duration).UnixNano(),
		Value:      value,
	})
	c.performForceRecachingIfNecessary()
}

// Use it in put methods to limit sudden growth of RAM cache size.
func (c *Cache) performForceRecachingIfNecessary() {
	var needsRecaching bool
	c.ramCache.RLock()
	needsRecaching = len(c.ramCache.items) > c.ramCache.maxCount*2
	c.ramCache.RUnlock()
	if needsRecaching {
		go c.recache()
	}
}

func (c *Cache) CountOnDisk() int {
	return c.diskCache.Count()
}

func (c *Cache) CountInRAM() int {
	return c.ramCache.Count()
}

func (c *Cache) Count() int {
	return c.CountInRAM() + c.CountOnDisk()
}

func (c *Cache) garbageCollector() {
	for {
		<-time.After(c.cleanupInterval)

		c.ramCache.collectGarbage()
		c.ramCache.collectGarbage()

		c.recache()
	}
}

func (c *Cache) GetExpiration(key string) (int64, error) {
	item, err := c.GetRawItem(key)
	if err != nil {
		return 0, err
	}
	return item.Expiration, nil
}

func (c *Cache) Get(key string, valueType reflect.Type) (value interface{}, err error) {
	var item *Item
	item, err = c.GetRawItem(key)
	if err != nil {
		return
	}

	_value := reflect.ValueOf(item.Value)
	if _value.Type().Name() == valueType.Name() {
		return item.Value, nil
	}

	switch item.Value.(type) {
	case map[string]interface{}:
		if valueType.Kind() == reflect.Struct || valueType.Kind() == reflect.Map {
			switch reflect.New(valueType).Interface().(type) {
			case map[string]interface{}:
				return item.Value, nil
			default:
				// marshal back to json and unmarshal to target type
				data, err := json.Marshal(item.Value)
				if err != nil {
					return nil, fmt.Errorf("remarshalling error: %s", err)
				}
				value = reflect.New(valueType).Interface()
				if err = json.Unmarshal(data, &value); err != nil {
					return nil, fmt.Errorf("unmarshalling error: %s", err)
				}
				return value, nil
			}
		} else {
			return nil, invalidValueType
		}
	default:
		if !_value.Type().ConvertibleTo(valueType) {
			return nil, invalidValueType
		}
		return _value.Convert(valueType), nil
	}
}

func (c *Cache) GetInt(key string) (int, error) {
	item, err := c.GetRawItem(key)
	if err != nil {
		return 0, err
	}
	switch item.Value.(type) {
	case int:
		return item.Value.(int), nil
	case float64:
		return int(item.Value.(float64)), nil
	}
	return 0, invalidValueType
}

func (c *Cache) GetInt64(key string) (int64, error) {
	item, err := c.GetRawItem(key)
	if err != nil {
		return 0, err
	}
	switch item.Value.(type) {
	case int64:
		return item.Value.(int64), nil
	case float64:
		return int64(item.Value.(float64)), nil
	}
	return 0, invalidValueType
}

func (c *Cache) GetUint(key string) (uint, error) {
	item, err := c.GetRawItem(key)
	if err != nil {
		return 0, err
	}
	switch item.Value.(type) {
	case uint:
		return item.Value.(uint), nil
	case float64:
		return uint(item.Value.(float64)), nil
	}
	return 0, invalidValueType
}

func (c *Cache) GetUint64(key string) (uint64, error) {
	item, err := c.GetRawItem(key)
	if err != nil {
		return 0, err
	}
	switch item.Value.(type) {
	case uint64:
		return item.Value.(uint64), nil
	case float64:
		return uint64(item.Value.(float64)), nil
	}
	return 0, invalidValueType
}

func (c *Cache) GetFloat64(key string) (float64, error) {
	item, err := c.GetRawItem(key)
	if err != nil {
		return 0, err
	}
	return item.Value.(float64), nil
}

func (c *Cache) GetFloat32(key string) (float32, error) {
	item, err := c.GetRawItem(key)
	if err != nil {
		return 0, err
	}
	switch item.Value.(type) {
	case float32:
		return item.Value.(float32), nil
	case float64:
		return float32(item.Value.(float64)), nil
	}
	return 0, invalidValueType
}

func (c *Cache) GetString(key string) (string, error) {
	item, err := c.GetRawItem(key)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%s", item.Value), nil
}

func (c *Cache) GetBool(key string) (bool, error) {
	item, err := c.GetRawItem(key)
	if err != nil {
		return false, err
	}
	return item.Value.(bool), nil
}

type sortingItems struct {
	LastUse    int64
	Expiration int64
	Key        string
}

func (c *Cache) recache() {
	c.ramCache.Lock()
	defer c.ramCache.Unlock()

	sliceOfSortingItemsToItemsMap := func(s []sortingItems) map[string]Item {
		result := make(map[string]Item)
		for _, item := range s {
			result[item.Key] = Item{
				Expiration: item.Expiration,
				Value:      c.ramCache.items[item.Key].Value,
			}
		}
		return result
	}

	if len(c.ramCache.items) > c.ramCache.maxCount {
		// first sort all
		var items []sortingItems
		for k, v := range c.ramCache.items {
			items = append(items, sortingItems{
				LastUse:    v.LastUse,
				Expiration: v.Expiration,
				Key:        k,
			})
		}
		sort.Slice(items, func(i, j int) bool {
			return items[i].LastUse < items[j].LastUse
		})

		// then get a quarter of the result (most unusable items) if number of extra items is less
		extra := len(items) - c.ramCache.maxCount
		quarter := len(items) / 4
		if quarter > extra {
			items = items[:quarter]
		} else {
			items = items[:extra]
		}

		// divide all by groups by expiration to optimize disk cache garbage collector
		var zeroExpirationItems []sortingItems
		var nonZeroExpirationItems []sortingItems
		for _, item := range items {
			if item.Expiration == 0 {
				zeroExpirationItems = append(zeroExpirationItems, item)
			} else {
				nonZeroExpirationItems = append(nonZeroExpirationItems, item)
			}
		}
		sort.Slice(nonZeroExpirationItems, func(i, j int) bool {
			return items[i].Expiration < items[j].Expiration
		})
		var nonZeroExpirationItemsGroups [][]sortingItems
		var zeroExpirationItemsGroups [][]sortingItems

		smartSplit := func(source []sortingItems, target *[][]sortingItems, groupSize int) {
			if count := len(source); count > groupSize*2 {
				for i := 0; i < count; i += groupSize {
					pos := i + groupSize
					if pos > count {
						pos = count
					}
					*target = append(*target, source[i:pos])
				}
			}
		}
		smartSplit(nonZeroExpirationItems, &nonZeroExpirationItemsGroups, 20)
		smartSplit(zeroExpirationItems, &zeroExpirationItemsGroups, 20)

		put := func(items []sortingItems) {
			err := c.diskCache.Put(sliceOfSortingItemsToItemsMap(items))
			if err != nil {
				log.Println("[tlcache recaching] ERROR:", err)
			} else {
				for _, item := range items {
					delete(c.ramCache.items, item.Key)
				}
			}
		}

		// put result to disk cache
		if nonZeroExpirationItemsGroups != nil {
			for _, group := range nonZeroExpirationItemsGroups {
				put(group)
			}
		} else if nonZeroExpirationItems != nil {
			put(nonZeroExpirationItems)
		}
		if zeroExpirationItemsGroups != nil {
			for _, group := range zeroExpirationItemsGroups {
				put(group)
			}
		} else if zeroExpirationItems != nil {
			put(zeroExpirationItems)
		}
	}
}
