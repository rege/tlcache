package tlcache

import (
	"fmt"
	"gitlab.com/rege/vgenerator"
	"math/rand"
	"testing"
)

func BenchmarkRamCache_Put(b *testing.B) {
	ramCache := newRamCache(10)
	for i := 0; i < b.N; i++ {
		ramCache.Put(fmt.Sprintf("item-%d", rand.Uint64()), &Item{
			0,
			vgenerator.GenRandomValue(),
		})
		if i%10 == 0 {
			ramCache.Flush()
		}
	}
}