package tlcache

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"path/filepath"
	"sync"
	"time"
)

type diskCache struct {
	sync.RWMutex
	directory string
	notes     []fileNote // the notes that are stored on disk and that contain items
}

type fileNote struct {
	expiration int64           // the last Expiration time of all items, that is contained it this note
	fPath      string          // filepath
	stored     map[string]bool // if Value=false then item is deleted
}

func newDiskCache(directory string) (*diskCache, error) {

	cache := diskCache{
		notes:     make([]fileNote, 0),
		directory: directory,
	}

	fileInfo, err := os.Stat(directory)
	if os.IsNotExist(err) {
		err = os.Mkdir(directory, os.ModePerm)
	}
	if err != nil {
		return nil, err
	}
	if fileInfo != nil && !fileInfo.IsDir() {
		return nil, errors.New("specified directory is not a directory")
	}

	return &cache, nil
}

func (d *diskCache) Flush() {
	d.Lock()
	defer d.Unlock()
	for _, note := range d.notes {
		_ = os.Remove(note.fPath)
	}
	d.notes = make([]fileNote, 0)
}

func (d *diskCache) Contains(key string) bool {
	for _, note := range d.notes {
		if exist, ok := note.stored[key]; exist && ok {
			return true
		}
	}
	return false
}

// If an item exists already, it will be overwritten.
func (d *diskCache) Put(items map[string]Item) error {

	var lastExpiration int64 = 0

	// keys index
	storedItems := make(map[string]bool)

	for key, item := range items {

		if lastExpiration < item.Expiration {
			lastExpiration = item.Expiration
		}

		if d.Contains(key) {
			d.Delete(key)
		}

		storedItems[key] = true
	}

	jsonData, err := json.Marshal(items)
	if err != nil {
		return err
	}

	// make unique fPath
	var filename string
	for {
		filename = fmt.Sprintf("cache-%x.json", rand.Uint64())
		d.RLock()
		for _, note := range d.notes {
			if filename == note.fPath {
				continue
			}
		}
		d.RUnlock()
		break
	}

	fPath := filepath.Join(d.directory, filename)

	err = ioutil.WriteFile(fPath, jsonData, os.ModePerm)
	if err != nil {
		return err
	}

	d.notes = append(d.notes, fileNote{
		expiration: lastExpiration,
		stored:     storedItems,
		fPath:      fPath,
	})

	return nil
}

func (d *diskCache) GetItem(key string) (*Item, error) {
	d.Lock()
	defer d.Unlock()
	for i := len(d.notes) - 1; i >= 0; i-- {
		if exist, ok := d.notes[i].stored[key]; ok && exist {
			m, err := d.notes[i].readItemsFromDisk()
			if err != nil {
				return nil, err
			}
			if targetDiskCacheItem, ok := m[key]; ok {
				if targetDiskCacheItem.Expiration < time.Now().UnixNano() {
					d.notes[i].stored[key] = false
					return nil, itemTimeIsOver
				}
				return &targetDiskCacheItem, nil
			}
		}
	}
	return nil, notFound
}

func (f *fileNote) readItemsFromDisk() (map[string]Item, error) {
	data, err := ioutil.ReadFile(f.fPath)
	if err != nil {
		return nil, err
	}

	var result map[string]Item
	err = json.Unmarshal(data, &result)
	return result, err
}

func (d *diskCache) Delete(key string) {
	d.Lock()
	defer d.Unlock()
	for i, note := range d.notes {
		if _, ok := note.stored[key]; ok {
			d.notes[i].stored[key] = false
		}
	}
}

// delete a note by its number in notes slice and delete a file that belongs to deleted note
func (d *diskCache) deleteNote(position int) {
	_ = os.Remove(d.notes[position].fPath)
	last := len(d.notes) - 1
	d.notes[position], d.notes[last] = d.notes[last], d.notes[position]
	d.notes = d.notes[:last]
}

func (d *diskCache) collectGarbage() {
	d.Lock()
	defer d.Unlock()

	for i, note := range d.notes {
		if note.expiration > 0 && time.Now().UnixNano() > note.expiration {
			d.deleteNote(i)
		}
	}
}

func (d *diskCache) Count() (count int) {
	d.RLock()
	defer d.RUnlock()
	for _, note := range d.notes {
		for _, exists := range note.stored {
			if exists {
				count++
			}
		}
	}
	return
}
