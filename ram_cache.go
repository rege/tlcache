package tlcache

import (
	"sync"
	"time"
)

type ramCache struct {
	sync.RWMutex
	maxCount int
	items    map[string]regularItem
}

func newRamCache(maxCount int) *ramCache {

	if maxCount < 100 {
		maxCount = 100
	}

	cache := ramCache{
		items:    make(map[string]regularItem),
		maxCount: maxCount,
	}

	return &cache
}

func (r *ramCache) Put(key string, item *Item) {

	r.Lock()
	defer r.Unlock()

	var ri regularItem
	ri.Value = item.Value
	ri.Expiration = item.Expiration
	ri.LastUse = time.Now().UnixNano()

	r.items[key] = ri
}

func (r *ramCache) GetItem(key string) (*Item, error) {
	// because it use mutex lock/unlock
	defer r.touchItem(key)

	r.Lock()
	defer r.Unlock()

	if item, ok := r.items[key]; ok {
		// if Expiration time is zero, the item never will be deleted
		if item.Expiration > 0 {
			if time.Now().UnixNano() > item.Expiration {
				return nil, itemTimeIsOver
			}
		}
		return &Item{
			Expiration: item.Expiration,
			Value:      item.Value,
		}, nil
	}

	return nil, notFound
}

// Updates .LastUse field of specified item by its key.
// Note: Look for mutex lock/unlock collision. Recommended to use it in first defer to call it last.
// Note: It works only with the items that are in RAM.
func (r *ramCache) touchItem(key string) {
	r.Lock()
	defer r.Unlock()

	if item, ok := r.items[key]; ok {
		item.LastUse = time.Now().UnixNano()
		r.items[key] = item
	}
}

func (r *ramCache) Get(key string) (interface{}, bool) {
	// because it use mutex lock/unlock
	defer r.touchItem(key)

	r.Lock()
	defer r.Unlock()

	if item, ok := r.items[key]; ok {
		// if Expiration time is zero, the item never will be deleted
		if item.Expiration > 0 {
			if time.Now().UnixNano() > item.Expiration {
				return nil, false
			}
		}

		return item.Value, true
	}
	return nil, false
}

func (r *ramCache) Delete(key string) {

	r.Lock()

	defer r.Unlock()

	delete(r.items, key)
}

func (r *ramCache) collectGarbage() {
	r.Lock()
	defer r.Unlock()

	for k, i := range r.items {
		if i.Expiration > 0 && time.Now().UnixNano() > i.Expiration {
			delete(r.items, k)
		}
	}
}

func (r *ramCache) Count() int {
	r.RLock()
	defer r.RUnlock()
	return len(r.items)
}

func (r *ramCache) Flush() {
	r.Lock()
	defer r.Unlock()
	r.items = make(map[string]regularItem)
}