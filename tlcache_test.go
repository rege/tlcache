package tlcache

import (
	"fmt"
	"testing"
	"time"
)

func TestNew(t *testing.T) {
	_, err := New(time.Second, time.Second, 100, "test_cache")
	if err != nil {
		t.Errorf("got error by New(time.Second, time.Second, 100, \"test_cache\"): %s", err)
	}
}

func Test_recache(t *testing.T) {
	cache, err := New(time.Minute, time.Second, 100, "test_cache")
	if err != nil {
		t.Fatalf("got error by New(time.Second, time.Second, 100, \"test_cache\"): %s", err)
	}
	//defer cache.Flush()

	for i := 0; i < 150; i++ {
		cache.Put(fmt.Sprintf("item%d", i), i)
	}

	got := cache.Count()
	if got != 150 {
		t.Errorf("cache.Count() = %d; want 150", got)
	}
	got = cache.CountInRAM()
	if got != 150 {
		t.Errorf("cache.CountInRAM() = %d; want 150", got)
	}

	time.Sleep(time.Second * 3 / 2)

	got = cache.Count()
	if got != 150 {
		t.Errorf("cache.Count() = %d; want 150", got)
	}
	got = cache.CountInRAM()
	if got != 100 {
		t.Errorf("cache.CountInRAM() = %d; want 100", got)
	}
	got = cache.CountOnDisk()
	if got != 50 {
		t.Errorf("cache.CountOnDisk() = %d; want 50", got)
	}
}

func TestCache_Get(t *testing.T) {
	cache, err := New(time.Minute, time.Second, 100, "test_cache")
	if err != nil {
		t.Fatalf("got error by New(time.Second, time.Second, 100, \"test_cache\"): %s\n", err)
	}
	defer cache.Flush()

	for i := 0; i < 200; i++ {
		cache.Put(fmt.Sprintf("item%d", i), i)
	}
	time.Sleep(time.Second * 3 / 2)

	got, err := cache.GetInt("item150")
	if err != nil {
		t.Errorf("got error by cache.GetInt(\"item150\"): %s\n", err)
	} else {
		if got != 150 {
			t.Errorf("cache.GetInt(\"item150\") gave %d; want 150", got)
		}
	}
}
